__author__ = 'cocoon'


import os
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from genjob import robot_job_config ,write_config



class Job(object):
    """

    """
    def __init__(self,jobs,job_name):
        """
        """
        self.name = job_name
        self.jobs = jobs
        self.data = self.jobs.data['jenkins']['jobs'][job_name]


    @property
    def parameters(self):
        return " ".join(self.data['parameters'])


    @property
    def base(self):
        """

        """
        path,name =os.path.split(self.jobs.path)
        return path + '/' + self.data['name']

    @property
    def description(self):
        return self.data['description']

    @property
    def pythonpath(self):
        """

        """
        path,name = os.path.split(self.jobs.path)
        return path





    @property
    def config(self):
        """
            return xml config
        """
        command = "rflaunch -j %s  %s" %  (self.jobs.destination_path,self.name)
        return robot_job_config(self.name,command,description=self.description)

    def filepath(self,jenkins_home):
        """

        """
        return "%s/jobs/%s/config.xml" % (jenkins_home,self.name)


    def write_config(self,jenkins_home,overwrite=False):
        """

        """
        write_config(self.filepath((jenkins_home)) , self.config , overwrite=overwrite)

    def create(self,jenkins_api ):
        """

        """
        j = jenkins_api.create_job(self.name,self.config)


class Jobs(object):
    """


    """
    def __init__(self,path,destination_path=None):
        """

        :param path: path of jobs yaml file
        :return:
        """
        self.path = path
        #if not destination_path:
        #    destination_path = self.path
        #self.destination_path = destination_path
        with open(path,"r") as fh:

            self.data = load( fh, Loader=Loader)


    @property
    def destination_path(self):
        """
            the final path for jobs.yaml
        :return:
        """
        return self.data['jenkins']['config']['path']


    def get(self,job_name):
        """

        :param job_name:
        :return:
        """
        return Job(self,job_name)
    job=get


    def iter(self):
        """
            return each Job object

        :return:
        """
        for job_name in sorted(self.data['jenkins']['jobs'].keys()):
            yield self.job(job_name)





# ...