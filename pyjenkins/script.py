#!usr/bin/env python
# -*- coding: UTF-8 -*-
"""pyjenkins
    import : import jobs defined in a jobs.yaml file into jenkins (/jenkins/jobs/$job/config.xml)

Usage:
  pyjenkins import [--jenkins=<jenkins_home>] [--force] <job_file>
  pyjenkins create job  --host=<host> [-d=<destination>]   <job_file>

Options:
  -h --help     Show this screen.
  --version     Show version.
  --jenkins=<jenkins_home>
  -f , --force  override existing job in jenkins
  -d --destination=<destination>   destination_file eg /tests/robotdemo/jobs.yaml

"""
import sys
import os
from docopt import docopt
from jobs import Jobs
from jenkinsapi.jenkins import Jenkins

version = '0.0.1'


# export JENKINS_HOME=/jenkins


def main():
    arguments = docopt(__doc__, version=version)

    jenkins_home = os.environ.get('JENKINS_HOME',None)
    if not jenkins_home :
        jenkins_home = arguments['--jenkins']




    if arguments['import']:
        # import a jobs.yaml into jenkins
        assert jenkins_home is not None
        jobs = Jobs(arguments['<job_file>'])

        for job in jobs.iter():
            print job
            print job.filepath(jenkins_home)
            print job.config

            job.write_config(jenkins_home , overwrite=arguments['--force'])
            continue

    elif arguments['create'] and arguments['job']:
        # import a jobs.yaml into jenkins
        jobs = Jobs(arguments['<job_file>'])

        jenkins_api = Jenkins(arguments['--host'])

        for job in jobs.iter():
            print job
            print job.filepath(jenkins_home)
            print job.config

            job.create(jenkins_api)
            continue




    return 0


if __name__ == '__main__':
    sys.exit(main())
