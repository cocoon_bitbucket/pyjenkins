#!usr/bin/env python
# -*- coding: UTF-8 -*-
"""
rflaunch : launch a pybot with specific environement


Usage:
    rflaunch [--jobs=<yamlfile>] [--argfile=<argfile>] [--outputdir=<outdir>] <job_name>
    rflaunch --version
    rflaunch (-h | --help)

Options:  -h --help     Show this screen.
    -h , --help   Show this help
    --version     Show version.
    -j , --jobs=<yamlfiles>     full path to jobs.yaml [default: /tests/jobs.yaml]
    -a , --argfile=<argfile>   a pybot argfile
    -c , --collection=<job_collection>  a job collection
    -o , --outputdir=<outdir>   dir to output result [default: ./]

"""
import sys
import os


from jobs import Jobs




def main():

    #print(arguments)

    arguments = docopt(__doc__, version='0.0.1')

    jobs_path = arguments['--jobs']

    jobs = Jobs(jobs_path)

    job = jobs.get(arguments['<job_name>'])

    #print job.parameters
    #print job.base

    cmd_args={
        'base' : job.base,
        'pythonpath' : "--pythonpath " + job.pythonpath,
        'outputdir'  : "--outputdir " + arguments['--outputdir'],
        'parameters' : job.parameters
    }


    # prepare command line
    #print cmd_args
    cmd = "pybot %(pythonpath)s %(outputdir)s %(parameters)s %(base)s" % cmd_args

    print cmd


    return os.system(cmd)





from docopt import docopt


if __name__ == '__main__':

    sys.exit(main())