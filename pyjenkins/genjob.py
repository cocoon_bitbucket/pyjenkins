__author__ = 'cocoon'


import os,errno

robot_job_pattern ="""\
<?xml version='1.0' encoding='UTF-8'?>
<project>
  <actions/>
  <description>%(DESCRIPTION)s</description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <scm class="hudson.scm.NullSCM"/>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <triggers/>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.tasks.Shell>
      <command>%(COMMAND)s</command>
    </hudson.tasks.Shell>
  </builders>
  <publishers>
    <hudson.plugins.robot.RobotPublisher plugin="robot@1.4.3">
      <outputPath>./</outputPath>
      <reportFileName></reportFileName>
      <logFileName></logFileName>
      <outputFileName></outputFileName>
      <disableArchiveOutput>false</disableArchiveOutput>
      <passThreshold>%(passThreshold)s</passThreshold>
      <unstableThreshold>%(unstableThreshold)s</unstableThreshold>
      <otherFiles>
        <string>*.png</string>
      </otherFiles>
      <onlyCritical>true</onlyCritical>
    </hudson.plugins.robot.RobotPublisher>
  </publishers>
  <buildWrappers/>
</project>

"""

path = "/jenkins/jobs/%s/config.xml"

command = 'rflaunch -j /tests/demo/jobs.yaml keyword_driven'


def robot_job_filepath(job_name,jenkins_home="/jenkins"):
    """

    :return:
    """
    filepath = "%s/jobs/%s/config.xml" % (jenkins_home,job_name)
    return filepath


def robot_job_config(job_name,command,description="",passThreshold=10.0 ,unstableThreshold=50.0):
    """

    :param job_name: str
    :param command: str eg "rflaunch -j /tests/robotdemo/jobs.yaml "
    :return:
    """


    data = dict(
        COMMAND  =  command ,
        DESCRIPTION = description,
        passThreshold = passThreshold,
        unstableThreshold = unstableThreshold,
        )

    xml = robot_job_pattern % data

    return xml



def write_config(filepath,content,overwrite=False):
    """

    :param filepath:
    :param content:
    :param force:
    :return:
    """

    try:
        os.stat(filepath)
    except :
        # file does not exists : ok to create
        pass
    else:
        # file already exists
        if not overwrite:
            # ok to force over
            raise RuntimeError("config already exists %s" % filepath)

    # create dir ( /jenkins/jobs/$job )
    basedir,filename = os.path.split(filepath)

    try:
        os.makedirs(basedir)
    except OSError, e:
        # be happy if someone already created the path
        if e.errno != errno.EEXIST:
            raise

    # create file
    with open(filepath,"w") as fh:
        fh.write(content)
    return


if __name__ == "__main__":


    xml = robot_job_config('keyword_driven')
    path = robot_job_filepath('keyword_driven','/jenkins')

    print path
    print xml