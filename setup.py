from distutils.core import setup

setup(
    name='pyjenkins',
    version='0.0.1',
    packages=['pyjenkins'],
    url='',
    license='',
    author='cocoon',
    author_email='tordjman.laurent@gmail.com',
    description='tool for jenkins and robotframework',
    entry_points = {
        'console_scripts': ['rflaunch=pyjenkins.rflaunch:main','pyjenkins=pyjenkins.script:main']
    },
)
